#!/opt/local/bin/python2.7
"""
Export N900 call logs for importing to other platforms.

Migrate and transform call logs from n900/Maemo event logs to an XML
intermediate suitable for importing with
"""

# For filesystem stuff
import os

# Connect to DB and read its contents
import sqlite3

# For time conversions
import datetime

# Used in generating XML
import uuid


# Variables

db_archive_file = '/Users/kgoetz/n900 backup/Backup 22 August 2017//Root/home/user/.rtcom-eventlogger/backup.tgz'
db_file = '/Users/kgoetz/n900 backup/Backup 22 August 2017/Root/home/user/el-v1.db'
output_directory = '/Users/kgoetz/Google Drive/script-exported/telephony-data/'
output_filename = 'call-logs-output.xml'
# Anything before this won't be migrated
exclude_dates = datetime.datetime(2012, 5, 1, 0, 0)   # Seems to be roughly when sms' start being for me

if not db_file:
  os.system('tar xf "%s"' % db_archive_file)

# Set up connection to sqlite
# https://docs.python.org/2/library/sqlite3.html
conn = sqlite3.connect(db_file)
rdb = conn.cursor()

def create_output_path():
  # Create directory we will export to
  if not os.path.exists(output_directory):
      os.makedirs(output_directory)
      print 'Created %s' % output_directory


# I mapped out the data between db fields and xml, but see
# http://talk.maemo.org/showthread.php?t=64201 for more info from other people
message_data = rdb.execute('select storage_time, remote_uid, outgoing from Events where service_id = 1 and storage_time > {0};'.format(exclude_dates.strftime('%s'))).fetchall()
# print message_data
print len(message_data)

def process_message_data(message_data):
  xml_records = []

  for myrow in message_data:
    # print "================== New message =================="
    row_date = myrow[0]
    row_phone_number = myrow[1]
    row_presentation = 1
    if not row_phone_number :
      # NOTE: Based on my working assumption that 2 = private
      row_presentation = 2

    if row_phone_number:
      # We have a phone number so we can try and query for a name
      # print "Number is %s" % row_phone_number
      # TODO: This feels like something I should be able to query in message_data via joins "or something"
      contact_name_query = rdb.execute("select remote_name from Remotes where remote_uid = '%s';" % row_phone_number).fetchall()
    else:
      # No phone number means we can't look it up
      # print "No phone number specified"
      contact_name_query = []

  #   print "cnq: %s" % contact_name_query
  #   print len(contact_name_query)
    if len(contact_name_query) == 0 or contact_name_query[0][0] is None:
      row_contact_name = '(Unknown)'
    elif len(contact_name_query) == 1:
      row_contact_name = contact_name_query[0][0]
    elif len(contact_name_query) > 1:
      print "Contact number %s has multiple names associated with it. Using the first" % remote_uid
      row_contact_name = contact_name_query[0][0]

    if myrow[2]:  # If outgoing is 1 it must be a sent message
      row_type = 2
    else: # If not it must be received
      row_type = 1

    # Can't import duration - N900 doesn't store that data (end_time is unset for calls. Seriously)
    new_entry = '\t<call number="{0}" duration="{1}" date="{2}" type="{3}" presentation="{4}" readable_date="{5}" contact_name="{6}" />'.format(row_phone_number, 0, row_date, row_type, row_presentation, datetime.datetime.fromtimestamp(row_date), row_contact_name.encode('utf-8'))
    # print new_entry

    xml_records.append(new_entry)

    # print xml_records
    return xml_records

def build_xml_content(xml_records):
  xml_content = """<?xml version='1.0' encoding='UTF-8' standalone='yes' ?>
  <!--File Created By Karls horrible N900 data export script on {0}-->
  <?xml-stylesheet type="text/xsl" href="calls.xsl"?>
  <calls count="{1}" backup_set="{2}" backup_date="{3}">
  {4}
  </calls>
  """.format(datetime.datetime.now(), len(xml_records), uuid.uuid1(),
              datetime.datetime.now().strftime('%s'), '\n'.join(xml_records))
  return xml_content

def write_xml_file(xml_content):
  xml_file = open('{0}/{1}'.format(output_directory, output_filename), "w+")
  xml_file.write('{0}'.format(xml_content))
  xml_file.close()

# Actually do stuff
create_output_path()
xml_records = process_message_data(message_data)
content_for_file = build_xml_content(xml_records)
write_xml_file(content_for_file)

