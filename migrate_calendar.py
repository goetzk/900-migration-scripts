#!/opt/local/bin/python2.7
"""
Extract calendar data from backup sqlite file.

Exports as ical files which can be imported by calendaring programs.
"""

# For filesystem stuff
import os

# Connect to DB and read its contents
import sqlite3

# For time conversions
import datetime

# http://eventable.github.io/vobject/
import vobject

# Variables

# calendar_file = '/Users/kgoetz/n900 backup/Backup 22 August 2017//Root/home/user/.calendar/calendardb.backup'
calendar_file = '/Users/kgoetz/n900 backup/Backup august 2016/Root/home/user/.calendar/calendardb.backup'
output_directory = '/Users/kgoetz/Google Drive/script-exported/calendar-data/'
# Anything before this won't be migrated
exclude_dates = datetime.datetime(2012, 3, 1, 0, 0)

# Set up connection to sqlite
# https://docs.python.org/2/library/sqlite3.html
conn = sqlite3.connect(calendar_file)
rdb = conn.cursor()

# Query for names of calendars (Gathers a list of one item tuples)
calendar_names = rdb.execute("select Name from Calendars").fetchall()

# Read records from DB. Not directly in for loop so we can try/catch if inspired later.
calendar_entries = rdb.execute("select * from Components").fetchall()

def create_output_path():
  # Create directory we will export to
  if not os.path.exists(output_directory):
      os.makedirs(output_directory)
      print 'Created %s' % output_directory

def find_birthday_calendars(calendar_names):
  # So we don't do this each record, build a list of Calendar names which include birthday
  birthday_calendars = []
  for e in calendar_names:
    if e[0].lower().find('birthday') >= 0:
      birthday_calendars.append(e[0])
  return birthday_calendars

# Build a list of IDs from calendars with 'birthday' in the name
def is_birthday(data, birthday_calendars):
  """Establish if this record looks like a birthday."""
  if data[6].lower().find('birthday') >= 0:
    return True
  # I know this is poor form, but I don't want to manually type all 10 characters out!
  if data[6].lower().find('bithday') >= 0:
    return True

  if data[6].lower().find(' bday') >= 0:
    return True

  # Is the calendar ID of this event in our birthday_calendars list?
  if data[1] in birthday_calendars:
    return True

  # Fall through: not a birthday
  return False


# FIXME: has no error handling
def process_input(calendar_data):
  """Pre process list entries from calendar.

  Once processed data is sent on to my export functions

  All fields we change:
  4  DateStart INTEGER   - date as unix time or -1, needs conversion
  5  DateEnd INTEGER     - date as unix time or -1, needs conversion
  11 Until INTEGER       - date as unix time or -1, needs conversion unless we throw it away anyway
  13 CreatedTime INTEGER   - date as unix time, needs conversion
  14 ModifiedTime INTEGER  - date as unix time, needs conversion
  """
  working_data = list(calendar_data)

  # Mangle away!

  # Start always exists
  working_data[4] = datetime.datetime.fromtimestamp(working_data[4])

  if working_data[5] >= 0:
    working_data[5] = datetime.datetime.fromtimestamp(working_data[5])
  else:
    working_data[5] = 0
  if working_data[11] >= 0:
    working_data[11] = datetime.datetime.fromtimestamp(working_data[11])
  else:
    working_data[11] = 0
  if working_data[13] >= 0:
    working_data[13] = datetime.datetime.fromtimestamp(working_data[13])
  else:
    working_data[13] = 0
  if working_data[14] >= 0:
    working_data[14] = datetime.datetime.fromtimestamp(working_data[14])
  else:
    working_data[14] = 0

  working_data[12] = str(working_data[12])

  return working_data

# Create ical exporter
def export_to_ical(data):
  """Take data from sqlite and convert it to an ical file."""
  cal = vobject.iCalendar()
  cal.add('vevent')
  if data[4]:
    cal.vevent.add('dtstart').value = data[4]
  if data[5]:
    cal.vevent.add('dtend').value = data[5]
  if data[6]:
    # TODO: If 6 is empty... then what?
    cal.vevent.add('summary').value = data[6]
  if data[7]:
    cal.vevent.add('location').value = data[7]
  if data[8]:
    cal.vevent.add('description').value = data[8]
  if data[11]:
    print data[11]
    cal.vevent.add('rrule').value = 'FREQ=YEARLY;COUNT=100'
  if int(data[12]):
    cal.vevent.add('x-funambol-allday').value = data[12]
  if data[13]:
    cal.vevent.add('created').value = data[14]
  if data[14]:
    cal.vevent.add('last-modified').value = data[14]

  # print cal.serialize()
  # cal.prettyPrint()
  return cal

def save_as_ical(data, cal):
  cal_file = open('{0}/{1} - {2}.ics'.format(output_directory, data[14], data[6]), "w+")
  cal_file.write(cal.serialize())
  cal_file.close()

def export_to_vcard(data):
  """Take data from sqlite and convert it to a vcard file."""
  card = vobject.vCard()
  card.add('fn')
  card.fn.value = data[6]
  card.add('note')
  card.note.value = 'Imported from N900 via awful mangling script'
  # I need to do the formatting, library doesn't https://github.com/eventable/vobject/issues/93
  card.add('bday')
  card.bday.value = data[4].strftime('%Y%m%dT%H%m%S')  # Works when formatted, fails when unformatted.
  card.add('rev')
  card.rev.value = datetime.datetime.now().strftime('%Y%m%dT%H%m%S')  # Works when formatted, fails when unformatted.
  # print card.serialize()
  return card

def save_as_vcard(data, card):
  card_file = open('{0}/{1} - {2}.vcard'.format(output_directory, data[14], data[6]), "w+")
  card_file.write(card.serialize())
  card_file.close()

# Build this list before looping it
all_birthday_calendars = find_birthday_calendars(calendar_names)

# Make sure we have somewhere for our files to go
create_output_path()

# Loop the entries!
for record in calendar_entries:
  record_as_list = list(record)
  current_data = process_input(record_as_list)
  # Birthdays are to be exported as vcards to merge with existing users in Contacts
  if is_birthday(current_data, all_birthday_calendars):
    vcard_export = export_to_vcard(current_data)
    save_as_vcard(current_data, vcard_export)
  else:
    # And everything else as icals

    # if the event was created pre March 2012, don't export it.
    if current_data[4] > exclude_dates:
      ical_export = export_to_ical(current_data)
      save_as_ical(current_data, ical_export)
    else:
      print 'Skipped "%s" as it was older (%s) than the cut off date of %s' % (current_data[6], current_data[4], exclude_dates)

