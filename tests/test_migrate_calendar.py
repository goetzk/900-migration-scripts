
import mock

from .. import migrate_calendar

def test_is_birthday_contains_birthday():
  data = ['', '', '', '', '', '', 'blah birthday blah']
  birthday_calendars = []
  assert migrate_calendar.is_birthday(data, birthday_calendars) is True

def test_is_birthday_contains_bithday():
  data = ['', '', '', '', '', '', 'blah bithday blah']
  birthday_calendars = []
  assert migrate_calendar.is_birthday(data, birthday_calendars) is True

def test_is_birthday_contains_bday():
  data = ['', '', '', '', '', '', 'blah bday blah']
  birthday_calendars = []
  assert migrate_calendar.is_birthday(data, birthday_calendars) is True

def test_is_birthday_in_birthday_calendars():
  data = ['', 'birthday', '', '', '', '', 'blah nothing blah']
  birthday_calendars = ['no', 'yes', 'birthday']
  assert migrate_calendar.is_birthday(data, birthday_calendars) is True

def test_is_birthday_nothing_matched():
  data = ['', '', '', '', '', '', 'blah nothing blah']
  birthday_calendars = []
  assert migrate_calendar.is_birthday(data, birthday_calendars) is False


def test_find_birthday_calendars_empty_list():
  calendar_names = []
  assert migrate_calendar.find_birthday_calendars(calendar_names) == []

def test_find_birthday_calendars_not_birth_in_list():
  calendar_names = [(u'N900',)]
  assert migrate_calendar.find_birthday_calendars(calendar_names) == []

def test_find_birthday_calendars_birthday_in_list():
  calendar_names = [(u'Birthdays',)]
  assert migrate_calendar.find_birthday_calendars(calendar_names) == [u'Birthdays']


# TODO: testing this properly will be complicated - need to feed in data and
# see what it produces
# def test_process_input():


# TODO: Having trouble getting the mocks to work
# @mock.patch('migrate_calendar.os.path')
# @mock.patch('migrate_calendar.os')
# def test_create_output_path_doesnt_exist(mock_os, mock_os_path):
#   mock_os_path.exists.return_value = False
#   # Run code
#   migrate_calendar.create_output_path()
#
#   assert mock_os.makedirs.assert_called_with(output_directory)
#
# @mock.patch('migrate_calendar.os.path')
# @mock.patch('migrate_calendar.os')
# def test_create_output_path_does_exist(mock_os, mock_os_path):
#   mock_os_path.exists.return_value = True
#   # Run code
#   migrate_calendar.create_output_path()
#   assert mock_os.makedirs.called is False

